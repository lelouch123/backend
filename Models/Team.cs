using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace aspnet_core_jwt_authentication_api.Models
{
  public class Team
  {
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; }
    public string Name { get; set; }
    public string Departement { get; set; }
    public string Description { get; set; }
    public int Number { get; set; }
    public string Image { get; set; }
    public List<string> Members { get; set; }
  }
}