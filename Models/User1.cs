using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace aspnet_core_jwt_authentication_api.Models
{
  public class User1
  {
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; }
    public string name { get; set; }
    public string lastname { get; set; }
    public string email { get; set; }
    public string password { get; set; }
    public int age { get; set; }
    public string Token { get; set; }

    public string image { get; set; }

    public List<Roles> roles { get; set; }

    public Status status { get; set; } = new Status { };

  }
}