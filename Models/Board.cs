using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace aspnet_core_jwt_authentication_api.Models
{
  // settings: {color: "", subscribed: true, cardCoverImages: true}
  public class Settings
  {
    public string Color { get; set; }
    public Boolean Subscribed { get; set; }
    public Boolean CardCoverImages { get; set; }
  }
  // 0: List {id: "1eb036f7", name: "yo", idCards: Array(0)}
  public class List
  {

    public string Id { get; set; }
    public string Name { get; set; }

    public List<string> IdCards { get; set; }
  }

  public class Label
  {

    public string Id { get; set; }
    public string Name { get; set; }
    public string Color { get; set; }

  }
  public class Board
  {
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; }

    public List<Label> Labels { get; set; }

    public List<List> Lists { get; set; }
    public Settings Settings { get; set; }

    public string ProjectId { get; set; }


    public BsonDocument sections { get; set; } = new BsonDocument() { { "empty", new BsonDocument() } };
    public object obj = new object();

    public Boolean istemplate { get; set; } = false;

    public string Name { get; set; } = "";
  }
}