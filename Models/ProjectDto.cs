using System;
using System.Collections.Generic;

namespace aspnet_core_jwt_authentication_api.Models
{
  public class ProjectDto
  {
    public string company { get; set; }
    public string Title { get; set; } = string.Empty;
    public string Image { get; set; } = string.Empty;
    public string Projectoverview { get; set; } = string.Empty;
    public object sections { get; set; } = new object();
    public DateTime Startdate { get; set; }
    public DateTime Enddate { get; set; }
    public List<string> Teams { get; set; } = new List<string>();
    public string templateId { get; set; }
    public string responsable { get; set; } = "";

    public string template { get; set; } = string.Empty;
    public List<ResourceReservation> resources { get; set; } = new List<ResourceReservation>();
    public List<Action> Actions { get; set; } = new List<Action>();
  }
}