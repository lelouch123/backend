namespace aspnet_core_jwt_authentication_api.Models
{
  public class Dialog
  {
    public string who { get; set; } = "";
    public string message { get; set; } = "";
    public string time { get; set; } = "";


  }
}