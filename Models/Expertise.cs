using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace aspnet_core_jwt_authentication_api.Models
{
  public class Expertise
  {
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; }
    public string Maitrise { get; set; }
    public double Note { get; set; }
  }
}