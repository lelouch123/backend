using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace aspnet_core_jwt_authentication_api.Models
{
  public class Expectation
  {
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; }
    public string Customerexpectation { get; set; }
    public string Priority { get; set; }
    public DateTime Date { get; set; }
    public string Source { get; set; }
    public string Projected { get; set; }
  }
}