using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace aspnet_core_jwt_authentication_api.Models
{
  public class Risk
  {
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; }
    public string Description { get; set; }
    public int Impact { get; set; }
    public int Probability { get; set; }
    public string Action { get; set; }
    public string Projectid { get; set; }


  }
}