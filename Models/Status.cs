using System;

namespace aspnet_core_jwt_authentication_api.Models
{
  public class Status
  {
    public string Connectionid { get; set; } = "";
    public string State { get; set; } = "offline";

    public string Userid { get; set; } = "";
    public Boolean connected { get; set; } = false;
  }
}