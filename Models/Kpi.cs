using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace aspnet_core_jwt_authentication_api.Models
{
  public class field
  {
    public string name { get; set; }
    public string value { get; set; }

  }
  public class Kpi
  {
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; }
    public string name { get; set; }
    public string description { get; set; }
    public string target { get; set; }
    public string kpivalue { get; set; }
    public string formula { get; set; }
    public string projected { get; set; }
    public List<field> fields { get; set; }

  }
}