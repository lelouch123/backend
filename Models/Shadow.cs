using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace aspnet_core_jwt_authentication_api.Models
{
  public class Shadow
  {
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; }
    public string Entity { get; set; }

    public string Entityid { get; set; }
    public string type { get; set; }

    public string user { get; set; }
    public DateTime date { get; set; } = DateTime.UtcNow;
  }
}