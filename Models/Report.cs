using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace aspnet_core_jwt_authentication_api.Models
{
  public class Report
  {
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; }
    public string report { get; set; }
    public string description { get; set; }
    public string type { get; set; }
    public string senderid { get; set; }
    public string reciverid { get; set; }
    public DateTime date { get; set; } = DateTime.Now;

  }
}