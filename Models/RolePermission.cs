using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace aspnet_core_jwt_authentication_api.Models
{
  public class RolePermission
  {
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; }
    public string role { get; set; }
    public string description { get; set; }
    public List<string> permissions { get; set; } = new List<string>();
  }
}