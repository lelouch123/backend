using System;

namespace aspnet_core_jwt_authentication_api.Models
{
  public class ResourceReservation
  {
    public string Name { get; set; }

    public int Number { get; set; }
    public DateTime Startdate { get; set; }
    public DateTime Enddate { get; set; }

  }
}