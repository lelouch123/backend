using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace aspnet_core_jwt_authentication_api.Models
{
  public class Chats
  {
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; }

    public string Iduser1 { get; set; } = "";

    public string Iduser2 { get; set; } = "";

    public List<Dialog> dialog { get; set; } = new List<Dialog>();
  }
}