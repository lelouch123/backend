using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;

namespace aspnet_core_jwt_authentication_api.Hubs
{

  public class MessageHub : Hub
  {
    public Task SendMessageToAll(string message)
    {


      return Clients.All.SendAsync("ReceiveMessage", message);
    }
    public string GetConnectionId()
    {
      return Context.ConnectionId;
    }
  }
}