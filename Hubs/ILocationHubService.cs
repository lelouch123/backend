using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using WebApi.Services;

namespace aspnet_core_jwt_authentication_api.Hubs
{
  public interface ILocationHubService
  {
    Task GetLocation(string lat);

  }


  // public class LocationHub : Hub<ILocationHubService>
  public class LocationHub : Hub
  {
    private IUserService _userService;

    public LocationHub(IUserService userService)
    {
      _userService = userService;

    }


    public Task SendMessageToAll(string message)
    {
      return Clients.All.SendAsync("ReceiveMessage", message);
    }

    public string GetConnectionId(string msg)
    {
      var user = _userService.GetUser(msg);

      if (user != null)
      {
        user.status.Userid = user.Id;
        user.status.connected = true;
        user.status.Connectionid = Context.ConnectionId;
        user.status.State = "online";
        _userService.EditUser(msg, user);
      }
      var users = _userService.contacts();

      Clients.All.SendAsync("getContacts", users);
      return Context.ConnectionId + msg;
    }


    public Task SendMessageToOne(string user, Object msg)
    {
      // return Clients.All.SendAsync("ReceiveMessage", "Message To one =" + user);

      return Clients.Client(user).SendAsync("ReceiveMessage1", msg);
    }
    public Task SendReportToOne(string user, Object msg)
    {
      // return Clients.All.SendAsync("ReceiveMessage", "Message To one =" + user);

      return Clients.Client(user).SendAsync("ReceiveReport", msg);
    }

    public Task GetContacts()
    {
      var users = _userService.contacts();

      return Clients.All.SendAsync("getContacts", users);
    }
    public override async Task OnConnectedAsync()
    {


      await Clients.All.SendAsync("UserDisconnected", Context.ConnectionId);
      await base.OnConnectedAsync();
    }
    public override async Task OnDisconnectedAsync(Exception ex)
    {
      var user = _userService.GetUserbyconnectionid(Context.ConnectionId);

      if (user != null)
      {
        user.status.Userid = user.Id;
        user.status.connected = false;
        user.status.Connectionid = "";
        user.status.State = "offline";
        _userService.EditUser(user.Id, user);
      }
      var users = _userService.contacts();

      await Clients.All.SendAsync("getContacts", users);
      await Clients.All.SendAsync("UserDisconnected", Context.ConnectionId);
      await base.OnDisconnectedAsync(ex);
    }
  }
}