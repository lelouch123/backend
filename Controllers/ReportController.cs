using aspnet_core_jwt_authentication_api.Models;
using aspnet_core_jwt_authentication_api.Services;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class ReportController : ControllerBase
  {
    private IReportService _riskService;
    public ReportController(IReportService riskService)
    {
      _riskService = riskService;

    }


    [HttpGet("all/{id:length(24)}")]
    public IActionResult GetAll(string id)
    {

      var risks = _riskService.all(id);
      return Ok(risks);
    }
    [HttpPost("create")]
    public IActionResult Create([FromBody] Report report)
    {
      _riskService.Create(report);
      return Ok(report);
    }



  }
}