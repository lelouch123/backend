using aspnet_core_jwt_authentication_api.Models;
using aspnet_core_jwt_authentication_api.Services;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class ShadowController : ControllerBase
  {
    private IShadowService _shadowService;
    public ShadowController(IShadowService shadowService)
    {
      _shadowService = shadowService;

    }
    [HttpPost()]
    public IActionResult create([FromBody] Shadow shadow)
    {
      _shadowService.Create(shadow);
      return Ok(shadow);
    }

    [HttpGet("{id:length(24)}")]
    public IActionResult getbyEnitiyId(string id)
    {
      var Shadows = _shadowService.Find(id);
      return Ok(Shadows);
    }
    [HttpGet("all")]
    public IActionResult getAll()
    {
      var Shadows = _shadowService.Findall();
      return Ok(Shadows);
    }
  }
}