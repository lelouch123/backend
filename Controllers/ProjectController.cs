using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using aspnet_core_jwt_authentication_api.Models;
using aspnet_core_jwt_authentication_api.Services;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using Newtonsoft.Json;
using WebApi.Services;

namespace WebApi.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class ProjectController : ControllerBase
  {
    private IProjectService _projectService;
    private readonly ITeamService _teamService;
    private IUserService _userService;

    private IBoardService _boardService;
    public ProjectController(IProjectService projectService, IBoardService boardService, ITeamService teamService, IUserService userService)
    {
      _projectService = projectService;
      _boardService = boardService;
      _teamService = teamService;
      _userService = userService;


    }

    [HttpPost("create")]
    public IActionResult Create([FromBody] ProjectDto project)
    {


      var proj = _projectService.Create(new Project
      {
        company = project.company,
        Title = project.Title,
        Image = project.Image,
        Startdate = project.Startdate,
        Enddate = project.Enddate,
        Teams = project.Teams,
        Projectoverview = project.Projectoverview,
        resources = project.resources
        ,
        sections = BsonDocument.Parse(project.sections.ToString()),
        responsableid = project.responsable,
      });


      proj.templateId = CreatB(project.template, proj.Id.ToString()); ;

      _projectService.update(proj.Id.ToString(), proj);

      return Ok(new { message = "success", id = proj.Id });
    }

    [HttpGet("all")]
    public IActionResult Getall()
    {
      var projects = _projectService.all();
      return Ok(projects);
    }
    [HttpGet("director/{id:length(24)}")]
    public IActionResult director(string id)
    {
      var projects = _projectService.director(id);
      return Ok(projects);
    }
    [HttpGet("{id:length(24)}")]
    public IActionResult Getone(string id)
    {
      var projects = _projectService.getone(id);
      var j = new object();
      j = ToJson(projects.sections);

      projects.obj = j;
      return Ok(projects);
    }

    [HttpDelete("{id:length(24)}")]
    public IActionResult Delte(string id)
    {
      var projects = _projectService.getone(id);
      if (projects == null)
      {
        return BadRequest(new { message = "project not exist" });
      }
      _boardService.delete(projects.templateId);
      _projectService.delete(id);
      return Ok(new { msg = "succes" });
    }
    [HttpPut("{id:length(24)}")]
    public IActionResult Editone(string id, [FromBody] ProjectDto project)
    {
      var p = new Project
      {
        company = project.company,
        Title = project.Title,
        Startdate = project.Startdate,
        Enddate = project.Enddate,
        Projectoverview = project.Projectoverview,
        Image = project.Image,
        Teams = project.Teams,
        templateId = project.templateId,
        sections = BsonDocument.Parse(project.sections.ToString())
      };
      _projectService.update(id, p);
      return Ok(new { message = "success" });
    }

    [HttpPut("action/{id:length(24)}/{actionid}")]


    public IActionResult VerifieAction(string id, string actionid)


    {
      var p = _projectService.getone(id);
      // if (!p.Actions.Contains(actionid))
      // {
      //   p.Actions.Add(actionid);
      //   _projectService.update(id, p);
      // }
      var v = false;
      foreach (aspnet_core_jwt_authentication_api.Models.Action act in p.Actions)
      {
        if (act.id == actionid)
          v = true;
      }
      if (!v)
      {
        p.Actions.Add(new aspnet_core_jwt_authentication_api.Models.Action { id = actionid, verfied = false });
        _projectService.update(id, p);
      }
      return Ok(new { message = "success" });

    }


    [HttpPut("verfication/{id:length(24)}/{actionid}")]


    public IActionResult VerifieAction1(string id, string actionid, [FromBody] actiondto bo)
    {
      var p = _projectService.getone(id);
      foreach (aspnet_core_jwt_authentication_api.Models.Action act in p.Actions)
      {
        if (act.id == actionid)
          act.verfied = bo.boo;

      }
      _projectService.update(id, p);
      return Ok(new { message = "hello" });

    }
    /////////////////// Board ///////////////////////
    [HttpPost("Board")]
    public IActionResult CreateBoard([FromBody] Board Board)
    {

      Board.sections = BsonDocument.Parse(Board.obj.ToString());
      var j = new object();
      j = ToJson(Board.sections);
      Board.obj = j;
      _boardService.Create(Board);

      return Ok(Board);
    }
    public string CreatB(string id, string projectid)
    {
      var newboard = _boardService.GetBoard(id);


      var Board = _boardService.Create(new Board
      {
        Lists = newboard.Lists,
        Labels = newboard.Labels,

        Settings = newboard.Settings,
        ProjectId = projectid
      });
      return Board.Id;
    }

    [HttpPost("Board/template")]
    public IActionResult CreateTemplate([FromBody] Board Board)
    {

      Board.istemplate = true;
      Board = _boardService.Create(new Board
      {
        Lists = Board.Lists,
        Labels = Board.Labels,
        Name = Board.Name,
        Settings = Board.Settings,
        istemplate = true
      });

      return Ok(Board);
    }

    [HttpGet("Board/{id:length(24)}")]
    public IActionResult GetBoard(String id)
    {

      var board = _boardService.GetBoard(id);
      var j = new object();
      j = ToJson(board.sections);
      board.obj = j;
      var project = _projectService.getone(board.ProjectId);
      var teams = project.Teams;

      IEnumerable<string> member = new List<string>();

      foreach (var Team in teams)
      {
        var me = _teamService.GetOne(Team);
        member = member.Union(me.Members);
      }
      var members = _userService.allusers();
      var filtredmembers = new List<User1>();
      foreach (var m in members)
      {
        if (member.Any(x => x == m.Id))
        {
          m.password = null;
          filtredmembers.Add(m);

        }
      }
      return Ok(new { board = board, name = project.Title, members = filtredmembers });
    }

    [HttpPut("Board/update")]
    public IActionResult UpdateBoard([FromBody] Board Board)
    {
      var newboard = _boardService.GetBoard(Board.Id);
      newboard.Labels = Board.Labels;
      newboard.Lists = Board.Lists;
      newboard.Settings = Board.Settings;
      newboard.sections = BsonDocument.Parse(Board.obj.ToString());
      newboard.obj = "";
      _boardService.update(Board.Id, newboard);
      return Ok(newboard);
    }

    [HttpDelete("Board/delete/{id:length(24)}")]
    public IActionResult DeleteBoard(String id)
    {
      _boardService.delete(id);
      return Ok(new { msg = "success" });
    }
    [HttpGet("Board/template/{id:length(24)}")]
    public IActionResult GetTempalte(String id)
    {

      var board = _boardService.GetBoard(id);



      return Ok(new { board = board });
    }
    [HttpGet("Board/templates")]
    public IActionResult GetAllTempalte()
    {

      var boards = _boardService.all();



      return Ok(new { boards = boards });
    }
    [HttpGet("all/tasks")]
    public IActionResult alltask()
    {

      var boards = _boardService.alltask();
      boards.ForEach(board =>
      {
        var j = new object();
        j = ToJson(board.sections);
        board.obj = j;
      });


      return Ok(new { boards = boards });
    }
    [HttpPut("Template/update")]
    public IActionResult TempalteBoard([FromBody] Board Board)
    {
      var newboard = _boardService.GetBoard(Board.Id);
      newboard.Labels = Board.Labels;
      newboard.Lists = Board.Lists;
      newboard.Settings = Board.Settings;
      newboard.Name = Board.Name;
      _boardService.update(Board.Id, newboard);
      return Ok(newboard);
    }

    ////////////////// Helpers ///////////////////////

    [HttpPost("images"), DisableRequestSizeLimit]
    public IActionResult Upload()
    {
      try
      {
        var file = Request.Form.Files[0];
        var folderName = Path.Combine("Resources", "Images");
        var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

        if (file.Length > 0)
        {
          var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');

          var fullPath = Path.Combine(pathToSave, fileName);
          var dbPath = Path.Combine(folderName, fileName);

          using (var stream = new FileStream(fullPath, FileMode.Create))
          {
            file.CopyTo(stream);
          }

          return Ok(new { dbPath });
        }
        else
        {
          return BadRequest();
        }
      }
      catch (Exception ex)
      {
        return StatusCode(500, $"Internal server error: {ex}");
      }
    }



    public string ToJson(BsonDocument bson)
    {
      using (var stream = new MemoryStream())
      {
        using (var writer = new BsonBinaryWriter(stream))
        {
          BsonSerializer.Serialize(writer, typeof(BsonDocument), bson);
        }
        stream.Seek(0, SeekOrigin.Begin);
        using (var reader = new Newtonsoft.Json.Bson.BsonReader(stream))
        {
          var sb = new StringBuilder();
          var sw = new StringWriter(sb);
          using (var jWriter = new JsonTextWriter(sw))
          {
            jWriter.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
            jWriter.WriteToken(reader);
          }
          return sb.ToString();
        }
      }
    }


  }
}