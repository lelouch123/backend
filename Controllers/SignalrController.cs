using System;
using aspnet_core_jwt_authentication_api.Hubs;
using aspnet_core_jwt_authentication_api.Models;
using aspnet_core_jwt_authentication_api.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace WebApi.Controllers
{
  [Authorize]
  [ApiController]
  [Route("[controller]")]

  public class SignalrController : ControllerBase
  {
    private IHubContext<LocationHub> _hubContext;
    private IChatService _chatservice;
    public SignalrController(IHubContext<LocationHub> hubContext, IChatService chatService)
    {
      _hubContext = hubContext;
      _chatservice = chatService;

    }
    [HttpGet("all/{user:length(24)}")]
    public IActionResult GetAll(string user)
    {
      var chats = _chatservice.getChats(user);
      return Ok(chats);
    }

    [HttpGet("create/{user1:length(24)}/{user2:length(24)}")]
    public IActionResult Create(string user1, string user2)
    {
      var chat = _chatservice.CreateChat(user1, user2);
      return Ok(chat);
    }

    [HttpPost("insert/{chatid:length(24)}")]
    public IActionResult insertdialog([FromBody] Dialog dialog, string chatid)
    {
      var chat = _chatservice.insertDialog1(dialog, chatid);
      return Ok(chat);
    }

    [HttpPost("signal")]
    public IActionResult Post([FromBody] string msg)
    {
      string currentUserId = User.Identity.Name;


      string retMessage = string.Empty;
      string id = string.Empty;
      try
      {
        // _hubContext.Clients.All.GetLocation(msg);

        retMessage = "Success";
      }
      catch (Exception e)
      {
        retMessage = e.ToString();
      }

      return Ok(currentUserId);
    }
  }
}