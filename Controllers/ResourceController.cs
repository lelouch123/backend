using Microsoft.AspNetCore.Mvc;
using aspnet_core_jwt_authentication_api.Services;
using aspnet_core_jwt_authentication_api.Models;

namespace WebApi.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class ResourceController : ControllerBase
  {
    private IResourceService _resourceService;
    public ResourceController(IResourceService resourceService)
    {
      _resourceService = resourceService;

    }

    [HttpPost("create")]
    public IActionResult Create([FromBody]Resource resource)
    {
      _resourceService.Create(resource);
      return Ok(resource);
    }

    [HttpGet("all")]
    public IActionResult GetAll()
    {
      var resources = _resourceService.all();
      return Ok(resources);
    }

    [HttpGet("detaill/{id:length(24)}")]
    public IActionResult detaill(string id)
    {
      var resource = _resourceService.Find(id);

      if (resource == null)
        return BadRequest(new { message = "resoruce not exists" });
      return Ok(resource);

    }

    [HttpDelete("delete/{id:length(24)}")]
    public IActionResult Delete(string id)
    {
      var resource = _resourceService.Find(id);

      if (resource == null)
        return BadRequest(new { message = "resoruce not exists" });
      _resourceService.delete(id);
      return Ok(new { msg = "succes" });
    }

    [HttpPut("edit/{id:length(24)}")]
    public IActionResult Edit(string id, [FromBody]Resource resource)
    {
      var res = _resourceService.Find(id);

      if (res == null)
        return BadRequest(new { message = "resoruce not exists" });
      resource.Id = res.Id;
      _resourceService.Edit(id, resource);
      return Ok(new { message = "success" });
    }


  }
}