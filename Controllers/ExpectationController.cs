using aspnet_core_jwt_authentication_api.Models;
using aspnet_core_jwt_authentication_api.Services;
using Microsoft.AspNetCore.Mvc;

namespace aspnet_core_jwt_authentication_api.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class ExpectationController : ControllerBase
  {
    private readonly IExpectationService _expectations;

    public ExpectationController(IExpectationService expectations)
    {
      _expectations = expectations;

    }
    [HttpPost("create")]
    public IActionResult Create(Expectation expectation)
    {
      _expectations.Create(expectation);
      return Ok(expectation);
    }
    [HttpGet("all")]
    public IActionResult getAll()
    {
      var expectations = _expectations.all();
      return Ok(expectations);
    }

    [HttpGet("allforproject/{id:length(24)}")]
    public IActionResult getAllForProject(string id)
    {
      var expectations = _expectations.allForproject(id);
      return Ok(expectations);
    }

    [HttpGet("{id:length(24)}")]
    public IActionResult getOne(string id)
    {
      var expectation = _expectations.Find(id);
      return Ok(expectation);
    }

    [HttpDelete("{id:length(24)}")]
    public IActionResult deletOne(string id)
    {
      var expectation = _expectations.Find(id);

      if (expectation == null)
        return BadRequest(new { message = "expectation not exists" });
      _expectations.delete(id);
      return Ok(new { message = "success" });
    }
    [HttpPut("{id:length(24)}")]
    public IActionResult edit(string id, [FromBody] Expectation expectation)
    {
      var expectationv = _expectations.Find(id);

      if (expectationv == null)
        return BadRequest(new { message = "expectation not exists" });
      expectation.Id = expectationv.Id;
      _expectations.Edit(id, expectation);
      return Ok(new { message = "success" });
    }
  }
}