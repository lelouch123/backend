﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using WebApi.Services;
using WebApi.Entities;
using aspnet_core_jwt_authentication_api.Models;
using aspnet_core_jwt_authentication_api.Entities;
using System.Collections.Generic;
using aspnet_core_jwt_authentication_api.Services;

namespace WebApi.Controllers
{
  [Authorize]
  [ApiController()]
  [Route("[controller]")]
  public class UsersController : ControllerBase
  {
    private IUserService _userService;
    private IPermissionService _permissionService;
    public UsersController(IUserService userService, IPermissionService permissionService)
    {
      _userService = userService;
      _permissionService = permissionService;
    }

    [AllowAnonymous]
    [HttpPost("authenticate")]
    public IActionResult Authenticate([FromBody]User userParam)
    {
      var user = _userService.Authenticate(userParam.Username, userParam.Password);

      if (user == null)
        return BadRequest(new { message = "Username or password is incorrect" });

      return Ok(user);
    }

    [HttpGet]
    public IActionResult GetAll()
    {
      var users = _userService.allusers();
      return Ok(users);
    }
    [HttpGet("detaill/{id:length(24)}")]
    public IActionResult GetOne(string id)
    {
      var user = _userService.GetUser(id);
      return Ok(user);
    }
    [HttpDelete("delete/{id:length(24)}")]
    public IActionResult Deleteone(string id)
    {
      var user = _userService.GetUser(id);
      if (user == null)
        return BadRequest(new { message = "user does not exists" });
      _userService.DeleteUser(id);
      return Ok(new { msg = "success" });
    }

    [HttpPut("edit/{id:length(24)}")]
    public IActionResult update(string id, [FromBody]User1 userToReplace)
    {
      var user = _userService.GetUser(id);
      if (user == null)
        return BadRequest(new { message = "user does not exists" });
      user.name = userToReplace.name;
      user.age = userToReplace.age;
      user.email = userToReplace.email;
      user.lastname = userToReplace.lastname;
      user.password = userToReplace.password;
      user.roles = userToReplace.roles;
      user.image = userToReplace.image;
      _userService.EditUser(id, user);
      return Ok(user);
    }

    // [HttpGet("test")]
    // [Authorize("Permissions.Dashboards.Edit")]

    // public IActionResult test()
    // {
    //   var users = _userService.GetAll();
    //   return Ok(users);
    // }

    [HttpPost("create")]
    public ActionResult<User1> Create([FromBody]User1 user)
    {
      _userService.Create(user);

      return Ok(user);
    }


    [HttpGet("all")]
    public ActionResult<List<User1>> all()
    {
      return Ok(_userService.allusers());
    }







    //role managment
    [HttpGet("roles/{id:length(24)}")]
    public IActionResult roles(string id)
    {
      var roles = _userService.GetRoles(id);
      return Ok(roles);
    }

    //Roles routes
    [HttpPost("roles/create")]
    public ActionResult Create([FromBody]RolePermission permission)
    {
      RolePermission role = null;
      if (permission.role != "")
        role = _permissionService.Find(permission.role);
      if (role != null)
        return BadRequest(new { message = "role allread exists" });

      _permissionService.Create(permission);
      return Ok(permission);
    }
    [AllowAnonymous]
    [HttpGet("roles/permissions/{role}")]
    public IActionResult permissions(string role)
    {
      var rol = _permissionService.Find(role);
      if (rol == null)
        return BadRequest(new { message = "role not exists" });

      var perms = _permissionService.allPermission(role);
      // if (perms == null)

      return Ok(perms);
    }

    [HttpGet("rolespermissions/{id:length(24)}")]
    public IActionResult rolespermissions(string id)
    {
      var roles = _userService.GetRoles(id);
      List<string> permissions = new List<string>();
      foreach (Roles r in roles)
      {
        var perms = _permissionService.allPermission(r.title);
        permissions.AddRange(perms);

      }
      return Ok(permissions);
    }

    [HttpGet("roles")]
    public IActionResult allroles()
    {

      return Ok(_permissionService.all());
    }
    [HttpGet("onerole/{id}")]
    public IActionResult getonerole(string id)
    {
      var rol = _permissionService.Findbyid(id);

      if (rol == null)
        return BadRequest(new { message = "role not exists" });
      return Ok(rol);

    }

    [HttpPut("roles/{id}")]
    public IActionResult upadatepermissions(string id, [FromBody]RolePermission permission)
    {
      var rol = _permissionService.Findbyid(id);

      if (rol == null)
        return BadRequest(new { message = "role not exists" });
      rol.permissions = permission.permissions;
      _permissionService.update(id, rol);
      // if (perms == null)
      // return Ok(rol);
      return Ok(new { message = "succes" });
    }
    [HttpPut("roleupdate/{id}")]
    public IActionResult upadaterole(string id, [FromBody]RolePermission permission)
    {
      var rol = _permissionService.Findbyid(id);

      if (rol == null)
        return BadRequest(new { message = "role not exists" });
      rol.role = permission.role;
      rol.description = permission.description;

      _permissionService.update(id, rol);
      // if (perms == null)
      // return Ok(rol);
      return Ok(new { message = "succes" });
    }


    [HttpDelete("roledelete/{id}")]
    public IActionResult deleteerole(string id)
    {
      var rol = _permissionService.Findbyid(id);

      if (rol == null)
        return BadRequest(new { message = "role not exists" });


      _permissionService.delete(id);
      // if (perms == null)
      // return Ok(rol);
      return Ok(new { message = "succes" });
    }
    /*
    some test
    */
    [HttpPost("dtotest")]

    public IActionResult Dto([FromBody]test t)
    {
      return Ok(new
      {
        message = t.message

      });
    }




  }
}
