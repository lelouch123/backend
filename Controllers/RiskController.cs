using aspnet_core_jwt_authentication_api.Models;
using aspnet_core_jwt_authentication_api.Services;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class RiskController : ControllerBase
  {
    private IRiskService _riskService;
    public RiskController(IRiskService riskService)
    {
      _riskService = riskService;

    }
    [HttpPost("create")]
    public IActionResult Create([FromBody] Risk risk)
    {
      _riskService.Create(risk);
      return Ok(risk);
    }

    [HttpGet("all")]
    public IActionResult GetAll()
    {

      var risks = _riskService.all();
      return Ok(risks);
    }

    [HttpGet("detaill/{id:length(24)}")]
    public IActionResult detaill(string id)
    {
      var risk = _riskService.Find(id);

      if (risk == null)
        return BadRequest(new { message = "risk not exists" });
      return Ok(risk);

    }
    [HttpDelete("delete/{id:length(24)}")]
    public IActionResult Delete(string id)
    {
      var risk = _riskService.Find(id);

      if (risk == null)
        return BadRequest(new { message = "risk not exists" });
      _riskService.delete(id);
      return Ok(new { msg = "succes" });
    }
    [HttpPut("edit/{id:length(24)}")]
    public IActionResult Edit(string id, [FromBody] Risk risk)
    {
      var res = _riskService.Find(id);

      if (res == null)
        return BadRequest(new { message = "risk not exists" });
      risk.Id = res.Id;
      _riskService.Edit(id, risk);
      return Ok(new { message = "success" });
    }
  }
}