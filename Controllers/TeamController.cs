using System.Collections.Generic;
using System.Linq;
using aspnet_core_jwt_authentication_api.Models;
using Microsoft.AspNetCore.Mvc;
using WebApi.Services;

namespace WebApi.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class TeamController : ControllerBase
  {
    private readonly ITeamService _teamService;
    public TeamController(ITeamService teamService)
    {
      _teamService = teamService;
    }

    [HttpPost]
    public IActionResult Create([FromBody] Team team)
    {
      _teamService.Create(team);
      return Ok(team);
    }

    [HttpGet("all")]
    public IActionResult All()
    {
      var teams = _teamService.GetAll();
      foreach (var element in teams)
      {
        element.Number = element.Members.Count();
      }
      return Ok(teams);
    }

    [HttpDelete("delete/{id:length(24)}")]
    public IActionResult Delete(string id)
    {
      var team = _teamService.GetOne(id);

      if (team == null)
        return BadRequest(new { message = "Team not exist" });
      _teamService.Delete(id);
      return Ok(new { msg = "success" });
    }

    [HttpGet("detaill/{id:length(24)}")]
    public IActionResult Detaill(string id)
    {
      var team = _teamService.GetOne(id);

      if (team == null)
        return BadRequest(new { message = "Team not exist" });

      return Ok(team);
    }

    [HttpPost("members")]
    public IActionResult members([FromBody] List<string> ids)
    {
      List<string> members = new List<string>();
      foreach (string s in ids)
      {
        var team = _teamService.GetOne(s);
        members = members.Union(team.Members).ToList();

      }



      return Ok(members);
    }

    [HttpPut("edit/{id:length(24)}")]
    public IActionResult update(string id, [FromBody] Team userToReplace)
    {
      var team = _teamService.GetOne(id);
      if (team == null)
        return BadRequest(new { message = "user does not exists" });
      team.Name = userToReplace.Name;
      team.Description = userToReplace.Description;
      team.Departement = userToReplace.Departement;
      team.Image = userToReplace.Image;

      _teamService.Editone(id, team);
      return Ok(team);
    }
    [HttpPut("editmembers/{id:length(24)}")]
    public IActionResult updatemembers(string id, [FromBody] Team userToReplace)
    {
      var team = _teamService.GetOne(id);
      if (team == null)
        return BadRequest(new { message = "user does not exists" });
      if (team.Members == null)
      {
        team.Members = new List<string>();
      }

      team.Members.AddRange(userToReplace.Members);


      _teamService.Editone(id, team);
      return Ok(team);
    }


    [HttpPut("editmembers1/{id:length(24)}")]
    public IActionResult updatemembers1(string id, [FromBody] Team userToReplace)
    {
      var team = _teamService.GetOne(id);
      if (team == null)
        return BadRequest(new { message = "user does not exists" });
      if (team.Members == null)
      {
        team.Members = new List<string>();
      }

      team.Members.Remove(userToReplace.Members[0]);


      _teamService.Editone(id, team);
      return Ok(team);
    }
  }
}