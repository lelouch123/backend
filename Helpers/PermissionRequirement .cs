using Microsoft.AspNetCore.Authorization;

namespace aspnet_core_jwt_authentication_api.Helpers
{
  internal class PermissionRequirement : IAuthorizationRequirement
  {
    public string Permission { get; private set; }

    public PermissionRequirement(string permission)
    {
      Permission = permission;
    }
  }
}