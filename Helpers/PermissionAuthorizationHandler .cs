using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using aspnet_core_jwt_authentication_api.Models;
using aspnet_core_jwt_authentication_api.Services;
using Microsoft.AspNetCore.Authorization;
using WebApi.Services;

namespace aspnet_core_jwt_authentication_api.Helpers
{
  internal class PermissionAuthorizationHandler : AuthorizationHandler<PermissionRequirement>
  {
    private IPermissionService _permissionService;
    private IUserService _userService;
    public PermissionAuthorizationHandler(IUserService userService, IPermissionService permissionService)
    {
      _userService = userService;
      _permissionService = permissionService;

    }

    protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, PermissionRequirement requirement)
    {
      //   !context.User.HasClaim(c => c.Issuer == "http://localhost:5000/" && c.Type == "office"))

      if (!context.User.HasClaim(c => c.Type == ClaimTypes.Name))
      {
        return;
      }
      var id = context.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Name).Value;

      // if (id == "1" && requirement.Permission == "Permissions.Dashboards.Create")
      // {
      //   context.Succeed(requirement);
      // }

      var roles = _userService.GetRoles(id);
      List<string> permissions = new List<string>();
      foreach (Roles r in roles)
      {
        var perms = _permissionService.allPermission(r.title);
        permissions.AddRange(perms);

      }

      foreach (string p in permissions)
      {
        if (requirement.Permission == p)
        {
          context.Succeed(requirement);
          return;
        }
      }

      // Get all the roles the user belongs to and check if any of the roles has the permission required
      // for the authorization to succeed.


      // var user = await _userManager.GetUserAsync(context.User);
      // var userRoleNames = await _userManager.GetRolesAsync(user);
      // var userRoles = _roleManager.Roles.Where(x => userRoleNames.Contains(x.Name));

      // foreach (var role in userRoles)
      // {
      //     var roleClaims = await _roleManager.GetClaimsAsync(role);
      //     var permissions = roleClaims.Where(x => x.Type == CustomClaimTypes.Permission &&
      //                                             x.Value == requirement.Permission &&
      //                                             x.Issuer == "LOCAL AUTHORITY")
      //                                 .Select(x => x.Value);

      //     if (permissions.Any())
      //     {
      //         context.Succeed(requirement);
      //         return;
      //     }
      // }


    }
  }
}

