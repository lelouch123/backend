using System.Collections.Generic;
using aspnet_core_jwt_authentication_api.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using WebApi.Helpers;

namespace aspnet_core_jwt_authentication_api.Services
{
  public interface IReportService
  {
    Report Create(Report report);
    List<Report> all(string id);
    Report Find(string id);
    void delete(string id);
    void Edit(string id, Report report);
  }
  public class ReportService : IReportService
  {
    private readonly IMongoCollection<Report> _reports;
    private readonly AppSettings _appSettings;

    public ReportService(IOptions<AppSettings> appSettings)
    {
      _appSettings = appSettings.Value;
      var client = new MongoClient("mongodb://redboudraa:test1234@cluster0-shard-00-00-klfli.mongodb.net:27017,cluster0-shard-00-01-klfli.mongodb.net:27017,cluster0-shard-00-02-klfli.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority");
      var database = client.GetDatabase("test");

      _reports = database.GetCollection<Report>("report");
    }
    public Report Create(Report report)
    {
      _reports.InsertOne(report);
      return report;
    }
    public List<Report> all(string id)
    {
      return _reports.Find(report => report.reciverid == id).ToList();
    }

    public Report Find(string id)
    {
      return _reports.Find(elt => elt.Id == id).FirstOrDefault();
    }

    public void delete(string id)
    {
      _reports.DeleteOne(elt => elt.Id == id);
    }

    public void Edit(string id, Report report)
    {
      _reports.ReplaceOne(elt => elt.Id == id, report);
    }
  }
}