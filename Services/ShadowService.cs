using System.Collections.Generic;
using aspnet_core_jwt_authentication_api.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using WebApi.Helpers;

namespace aspnet_core_jwt_authentication_api.Services
{
  public interface IShadowService
  {
    void Create(Shadow shadow);
    List<Shadow> Find(string id);
    List<Shadow> Findall();
  }
  public class ShadowService : IShadowService
  {
    private readonly IMongoCollection<Shadow> _shodows;
    private readonly AppSettings _appSettings;

    public ShadowService(IOptions<AppSettings> appSettings)
    {
      _appSettings = appSettings.Value;
      var client = new MongoClient("mongodb://redboudraa:test1234@cluster0-shard-00-00-klfli.mongodb.net:27017,cluster0-shard-00-01-klfli.mongodb.net:27017,cluster0-shard-00-02-klfli.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority");
      var database = client.GetDatabase("test");

      _shodows = database.GetCollection<Shadow>("shadows");
    }
    public void Create(Shadow shadow)
    {
      _shodows.InsertOne(shadow);

    }
    public List<Shadow> Find(string id)
    {
      return _shodows.Find(elt => elt.Entityid == id).ToList();
    }

    public List<Shadow> Findall()
    {
      return _shodows.Find(elt => true).ToList();
    }




  }
}