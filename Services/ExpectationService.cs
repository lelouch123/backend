using System.Collections.Generic;
using aspnet_core_jwt_authentication_api.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using WebApi.Helpers;

namespace aspnet_core_jwt_authentication_api.Services
{
  public interface IExpectationService
  {
    Expectation Create(Expectation expectation);
    List<Expectation> all();
    Expectation Find(string id);
    void delete(string id);
    void Edit(string id, Expectation expectation);
    List<Expectation> allForproject(string id);
  }
  public class ExpectationService : IExpectationService
  {
    private readonly IMongoCollection<Expectation> _expectations;
    public ExpectationService(IOptions<AppSettings> appSettings)
    {

      var client = new MongoClient("mongodb://redboudraa:test1234@cluster0-shard-00-00-klfli.mongodb.net:27017,cluster0-shard-00-01-klfli.mongodb.net:27017,cluster0-shard-00-02-klfli.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority");
      var database = client.GetDatabase("test");

      _expectations = database.GetCollection<Expectation>("expectation");
    }
    public Expectation Create(Expectation expectation)
    {
      _expectations.InsertOne(expectation);
      return expectation;
    }
    public List<Expectation> all()
    {
      return _expectations.Find(expectation => true).ToList();
    }
    public Expectation Find(string id)
    {
      return _expectations.Find(elt => elt.Id == id).FirstOrDefault();
    }
    public List<Expectation> allForproject(string id)
    {
      return _expectations.Find(elt => elt.Projected == id).ToList();
    }
    public void delete(string id)
    {
      _expectations.DeleteOne(elt => elt.Id == id);
    }
    public void Edit(string id, Expectation expectation)
    {
      _expectations.ReplaceOne(elt => elt.Id == id, expectation);
    }
  }
}