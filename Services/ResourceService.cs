using System.Collections.Generic;
using aspnet_core_jwt_authentication_api.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using WebApi.Helpers;

namespace aspnet_core_jwt_authentication_api.Services
{
  public interface IResourceService
  {
    Resource Create(Resource resource);
    List<Resource> all();
    Resource Find(string id);
    void delete(string id);
    void Edit(string id, Resource resource);
  }
  public class ResourceService : IResourceService
  {
    private readonly IMongoCollection<Resource> _resources;
    private readonly AppSettings _appSettings;

    public ResourceService(IOptions<AppSettings> appSettings)
    {
      _appSettings = appSettings.Value;
      var client = new MongoClient("mongodb://redboudraa:test1234@cluster0-shard-00-00-klfli.mongodb.net:27017,cluster0-shard-00-01-klfli.mongodb.net:27017,cluster0-shard-00-02-klfli.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority");
      var database = client.GetDatabase("test");

      _resources = database.GetCollection<Resource>("resource");
    }
    public Resource Create(Resource resource)
    {
      _resources.InsertOne(resource);
      return resource;
    }
    public List<Resource> all()
    {
      return _resources.Find(resource => true).ToList();
    }

    public Resource Find(string id)
    {
      return _resources.Find(elt => elt.Id == id).FirstOrDefault();
    }

    public void delete(string id)
    {
      _resources.DeleteOne(elt => elt.Id == id);
    }

    public void Edit(string id, Resource resource)
    {
      _resources.ReplaceOne(elt => elt.Id == id, resource);
    }
  }
}