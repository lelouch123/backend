using System.Collections.Generic;
using aspnet_core_jwt_authentication_api.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using WebApi.Helpers;

namespace WebApi.Services
{
  public interface ITeamService
  {
    Team Create(Team element);
    List<Team> GetAll();
    Team GetOne(string id);
    void Delete(string id);
    void Editone(string id, Team teamToReplace);
  }
  public class TeamService : ITeamService
  {
    private readonly IMongoCollection<Team> _teams;
    private readonly AppSettings _appSettings;

    public TeamService(IOptions<AppSettings> appSettings)
    {
      _appSettings = appSettings.Value;
      var client = new MongoClient("mongodb://redboudraa:test1234@cluster0-shard-00-00-klfli.mongodb.net:27017,cluster0-shard-00-01-klfli.mongodb.net:27017,cluster0-shard-00-02-klfli.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority");
      var database = client.GetDatabase("test");

      _teams = database.GetCollection<Team>("team");
    }
    public Team Create(Team element)
    {
      _teams.InsertOne(element);
      return element;
    }
    public List<Team> GetAll()
    {
      return _teams.Find(a => true).ToList();
    }
    public void Delete(string id)
    {
      _teams.FindOneAndDelete(user => user.Id == id);

    }

    public Team GetOne(string id)
    {
      return _teams.Find(user => user.Id == id).FirstOrDefault();
    }

    public void Editone(string id, Team teamToReplace)
    {
      _teams.FindOneAndReplace(user => user.Id == id, teamToReplace);

    }
  }
}