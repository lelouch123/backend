using System.Collections.Generic;
using aspnet_core_jwt_authentication_api.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using WebApi.Helpers;

namespace aspnet_core_jwt_authentication_api.Services
{
  public interface IPermissionService
  {
    RolePermission Create(RolePermission permission);
    List<string> allPermission(string role);
    RolePermission Find(string role);
    List<RolePermission> all();
    void update(string id, RolePermission perm);
    void delete(string id);
    RolePermission Findbyid(string id);
  }

  public class PermissionService : IPermissionService
  {
    private readonly IMongoCollection<RolePermission> _permissions;
    private readonly AppSettings _appSettings;

    public PermissionService(IOptions<AppSettings> appSettings)
    {
      _appSettings = appSettings.Value;
      var client = new MongoClient("mongodb://redboudraa:test1234@cluster0-shard-00-00-klfli.mongodb.net:27017,cluster0-shard-00-01-klfli.mongodb.net:27017,cluster0-shard-00-02-klfli.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority");
      var database = client.GetDatabase("test");

      _permissions = database.GetCollection<RolePermission>("permission");
    }

    public RolePermission Create(RolePermission permission)
    {
      _permissions.InsertOne(permission);
      return permission;
    }

    public List<string> allPermission(string role)
    {
      return _permissions.Find(book => book.role == role).FirstOrDefault().permissions;
    }

    public List<RolePermission> all()
    {
      return _permissions.Find(book => true).ToList();
    }

    public RolePermission Find(string role)
    {
      return _permissions.Find(permission => permission.role == role).FirstOrDefault();
    }

    public RolePermission Findbyid(string id)
    {
      return _permissions.Find(permission => permission.Id == id).FirstOrDefault();
    }
    public void update(string id, RolePermission perm)
    {
      _permissions.ReplaceOne(a => a.Id == id, perm);
    }

    private ObjectId GetInternalId(string id)
    {
      ObjectId internalId;
      if (!ObjectId.TryParse(id, out internalId))
        internalId = ObjectId.Empty;

      return internalId;
    }

    public void delete(string id)
    {
      _permissions.DeleteOne(a => a.Id == id);
    }
  }
}