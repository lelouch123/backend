using System.Collections.Generic;
using aspnet_core_jwt_authentication_api.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using WebApi.Helpers;

namespace aspnet_core_jwt_authentication_api.Services
{
  public interface IRiskService
  {
    Risk Create(Risk risk);
    List<Risk> all();
    Risk Find(string id);

    void delete(string id);

    void Edit(string id, Risk risk);
  }
  public class RiskService : IRiskService
  {
    private readonly IMongoCollection<Risk> _risks;
    private readonly AppSettings _appSettings;

    public RiskService(IOptions<AppSettings> appSettings)
    {
      _appSettings = appSettings.Value;
      var client = new MongoClient("mongodb://redboudraa:test1234@cluster0-shard-00-00-klfli.mongodb.net:27017,cluster0-shard-00-01-klfli.mongodb.net:27017,cluster0-shard-00-02-klfli.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority");
      var database = client.GetDatabase("test");

      _risks = database.GetCollection<Risk>("risk");
    }

    public Risk Create(Risk risk)
    {
      _risks.InsertOne(risk);
      return risk;
    }

    public List<Risk> all()
    {
      return _risks.Find(resource => true).ToList();
    }

    public Risk Find(string id)
    {
      return _risks.Find(elt => elt.Id == id).FirstOrDefault();
    }

    public void delete(string id)
    {
      _risks.DeleteOne(elt => elt.Id == id);
    }

    public void Edit(string id, Risk risk)
    {
      _risks.ReplaceOne(elt => elt.Id == id, risk);
    }
  }
}