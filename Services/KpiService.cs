using System.Collections.Generic;
using aspnet_core_jwt_authentication_api.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using WebApi.Helpers;

namespace aspnet_core_jwt_authentication_api.Services
{
  public interface IKpiService
  {
    Kpi Create(Kpi kpi);
    List<Kpi> all();
    Kpi Find(string id);
    void delete(string id);
    void Edit(string id, Kpi kpi);
    List<Kpi> Findbyproject(string id);
  }
  public class KpiService : IKpiService
  {
    private readonly IMongoCollection<Kpi> _kpis;
    private readonly AppSettings _appSettings;

    public KpiService(IOptions<AppSettings> appSettings)
    {
      _appSettings = appSettings.Value;
      var client = new MongoClient("mongodb://redboudraa:test1234@cluster0-shard-00-00-klfli.mongodb.net:27017,cluster0-shard-00-01-klfli.mongodb.net:27017,cluster0-shard-00-02-klfli.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority");
      var database = client.GetDatabase("test");

      _kpis = database.GetCollection<Kpi>("kpi");
    }
    public Kpi Create(Kpi kpi)
    {
      _kpis.InsertOne(kpi);
      return kpi;
    }
    public List<Kpi> all()
    {
      return _kpis.Find(kpi => true).ToList();
    }

    public Kpi Find(string id)
    {
      return _kpis.Find(elt => elt.Id == id).FirstOrDefault();
    }
    public List<Kpi> Findbyproject(string id)
    {
      return _kpis.Find(elt => elt.projected == id).ToList();
    }
    public void delete(string id)
    {
      _kpis.DeleteOne(elt => elt.Id == id);
    }

    public void Edit(string id, Kpi kpi)
    {
      _kpis.ReplaceOne(elt => elt.Id == id, kpi);
    }
  }
}