using System.Collections.Generic;
using aspnet_core_jwt_authentication_api.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using WebApi.Helpers;

namespace aspnet_core_jwt_authentication_api.Services
{
  public interface IProjectService
  {
    Project Create(Project project);
    List<Project> all();
    List<Project> director(string id);
    void update(string id, Project project);
    Project getone(string id);
    void delete(string id);
  }
  public class ProjectService : IProjectService
  {
    private readonly IMongoCollection<Project> _projects;
    public ProjectService(IOptions<AppSettings> appSettings)
    {

      var client = new MongoClient("mongodb://redboudraa:test1234@cluster0-shard-00-00-klfli.mongodb.net:27017,cluster0-shard-00-01-klfli.mongodb.net:27017,cluster0-shard-00-02-klfli.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority");
      var database = client.GetDatabase("test");

      _projects = database.GetCollection<Project>("project");
    }

    public Project Create(Project project)
    {
      _projects.InsertOne(project);
      return project;
    }

    public List<Project> all()
    {
      return _projects.Find(a => true).ToList();
    }
    public List<Project> director(string id)
    {
      return _projects.Find(a => a.responsableid == id).ToList();
    }
    public Project getone(string id)
    {
      ObjectId internalId = GetInternalId(id);

      return _projects.Find(a => a.Id == internalId).FirstOrDefault();
    }

    public void update(string id, Project project)
    {
      ObjectId internalId = GetInternalId(id);


      project.Id = internalId;

      _projects.ReplaceOne(a => a.Id == internalId, project);

    }

    public void delete(string id)
    {
      ObjectId internalId = GetInternalId(id);
      _projects.DeleteOne(a => a.Id == internalId);
    }
    private ObjectId GetInternalId(string id)
    {
      ObjectId internalId;
      if (!ObjectId.TryParse(id, out internalId))
        internalId = ObjectId.Empty;

      return internalId;
    }

  }
}