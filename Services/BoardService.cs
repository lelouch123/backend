using System.Collections.Generic;
using aspnet_core_jwt_authentication_api.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using WebApi.Helpers;

namespace aspnet_core_jwt_authentication_api.Services
{
  public interface IBoardService
  {
    Board Create(Board board);
    Board GetBoard(string id);
    void update(string id, Board board);
    List<Board> all();
    void delete(string id);
    List<Board> alltask();
  }
  public class BoardService : IBoardService
  {
    private readonly IMongoCollection<Board> _boards;
    public BoardService(IOptions<AppSettings> appSettings)
    {

      var client = new MongoClient("mongodb://redboudraa:test1234@cluster0-shard-00-00-klfli.mongodb.net:27017,cluster0-shard-00-01-klfli.mongodb.net:27017,cluster0-shard-00-02-klfli.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority");
      var database = client.GetDatabase("test");

      _boards = database.GetCollection<Board>("board");
    }

    public Board Create(Board board)
    {
      _boards.InsertOne(board);
      return board;
    }

    public Board GetBoard(string id)
    {
      return _boards.Find(a => a.Id == id).FirstOrDefault();

    }
    public void update(string id, Board board)
    {

      _boards.ReplaceOne(a => a.Id == id, board);

    }
    public List<Board> all()
    {
      return _boards.Find(a => a.istemplate == true).ToList();
    }
    public List<Board> alltask()
    {
      return _boards.Find(a => a.istemplate == false).ToList();
    }
    public void delete(string id)
    {

      _boards.DeleteOne(a => a.Id == id);
    }

  }
}