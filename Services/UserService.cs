using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using aspnet_core_jwt_authentication_api.Models;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using MongoDB.Driver;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Services
{


  public interface IUserService
  {
    User1 Authenticate(string username, string password);
    IEnumerable<User> GetAll();

    User1 Create(User1 user);

    List<User1> allusers();

    List<Roles> GetRoles(string id);

    User1 GetUser(string id);
    User1 GetUserbyconnectionid(string id);
    void DeleteUser(string id);

    void EditUser(string id, User1 userToReplace);

    List<User1> contacts();
  }

  public class UserService : IUserService
  {
    private readonly IMongoCollection<User1> _users1;

    // users hardcoded for simplicity, store in a db with hashed passwords in production applications
    private List<User> _users = new List<User>
        {
            new User { Id = 1, FirstName = "Test", LastName = "User", Username = "test", Password = "test" }
        };

    private readonly AppSettings _appSettings;

    public UserService(IOptions<AppSettings> appSettings)
    {
      _appSettings = appSettings.Value;
      var client = new MongoClient("mongodb://redboudraa:test1234@cluster0-shard-00-00-klfli.mongodb.net:27017,cluster0-shard-00-01-klfli.mongodb.net:27017,cluster0-shard-00-02-klfli.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority");
      var database = client.GetDatabase("test");

      _users1 = database.GetCollection<User1>("userstest");
    }

    public User1 Create(User1 user)
    {
      _users1.InsertOne(user);
      return user;
    }

    public List<User1> allusers()
    {
      return _users1.Find(book => true).ToList();
    }
    public List<User1> contacts()
    {
      return _users1.Find(book => true).ToList();
    }
    public List<Roles> GetRoles(string id)
    {
      return _users1.Find(user => user.Id == id).FirstOrDefault().roles;
    }

    public User1 GetUser(string id)
    {
      return _users1.Find(user => user.Id == id).FirstOrDefault();
    }

    public User1 GetUserbyconnectionid(string id)
    {
      return _users1.Find(user => user.status.Connectionid == id).FirstOrDefault();
    }
    public void EditUser(string id, User1 userToReplace)
    {
      _users1.FindOneAndReplace(user => user.Id == id, userToReplace);

    }
    public void DeleteUser(string id)
    {
      _users1.FindOneAndDelete(user => user.Id == id);

    }
    public User1 Authenticate(string username, string password)
    {

      var user1 = _users1.Find(x => x.email == username && x.password == password).FirstOrDefault();
      // return null if user not found

      if (user1 == null)
        return null;

      // authentication successful so generate jwt token
      var tokenHandler = new JwtSecurityTokenHandler();
      var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
      var tokenDescriptor = new SecurityTokenDescriptor
      {
        Subject = new ClaimsIdentity(new Claim[]
          {
                    new Claim(ClaimTypes.Name, user1.Id.ToString())
          }),
        Expires = DateTime.UtcNow.AddDays(7),
        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
      };
      var token = tokenHandler.CreateToken(tokenDescriptor);
      user1.Token = tokenHandler.WriteToken(token);

      // remove password before returning
      user1.password = null;

      return user1;
    }

    public IEnumerable<User> GetAll()
    {
      // return users without passwords
      return _users.Select(x =>
      {
        x.Password = null;
        return x;
      });
    }






  }
}