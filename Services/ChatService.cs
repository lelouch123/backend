using System.Collections.Generic;
using aspnet_core_jwt_authentication_api.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using WebApi.Helpers;

namespace aspnet_core_jwt_authentication_api.Services
{
  public interface IChatService
  {
    Chats insertDialog1(Dialog dialog, string chatid);
    Chats CreateChat(string user1, string user2);
    List<Chats> getChats(string user);

  }
  public class ChatService : IChatService
  {
    private readonly IMongoCollection<Chats> _chats;
    private readonly AppSettings _appSettings;

    public ChatService(IOptions<AppSettings> appSettings)
    {
      _appSettings = appSettings.Value;
      var client = new MongoClient("mongodb://redboudraa:test1234@cluster0-shard-00-00-klfli.mongodb.net:27017,cluster0-shard-00-01-klfli.mongodb.net:27017,cluster0-shard-00-02-klfli.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority");
      var database = client.GetDatabase("test");

      _chats = database.GetCollection<Chats>("chats");

    }

    public Chats insertDialog(Dialog dialog, string user1, string user2)
    {
      var cha = _chats.Find(elt => ((elt.Iduser1 == user1 && elt.Iduser2 == user2) || (elt.Iduser1 == user2 && elt.Iduser2 == user1))).FirstOrDefault();
      if (cha != null)
      {
        cha.dialog.Add(dialog);
        _chats.ReplaceOne(elt => elt.Id == cha.Id, cha);
        return cha;
      }
      return null;

    }
    public Chats insertDialog1(Dialog dialog, string chatid)
    {
      var cha = _chats.Find(elt => elt.Id == chatid).FirstOrDefault();
      if (cha != null)
      {
        cha.dialog.Add(dialog);
        _chats.ReplaceOne(elt => elt.Id == cha.Id, cha);
        return cha;
      }
      return null;

    }
    public Chats CreateChat(string user1, string user2)
    {
      var cha = _chats.Find(elt => ((elt.Iduser1 == user1 && elt.Iduser2 == user2) || (elt.Iduser1 == user2 && elt.Iduser2 == user1))).FirstOrDefault();
      if (cha == null)
      {
        cha = new Chats { Iduser1 = user1, Iduser2 = user2 };
        _chats.InsertOne(cha);
        return cha;
      }
      return cha;

    }

    public List<Chats> getChats(string user)
    {
      var chats = _chats.Find(elt => (elt.Iduser1 == user || elt.Iduser2 == user)).ToList();

      return chats;
    }

  }
}